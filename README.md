# `nginx`-based universal staticfiles server for Django

## Using the image

In your Django application, configure:
- `STATIC_URL = "static/"`
- `STATIC_ROOT = "/usr/src/static-root/static"`

Then run `collectstatic` in your Django app and mount the files in the server container (using your orchestrator of choice).

## Building the image

To see help:

```bash
./build-images.py --help
```

To build new images:

```bash
./build-images.py --base-tag 'registry.gitlab.com/arcanery/docker/django-nginx-staticfiles' [--push] [--no-cache]
```
