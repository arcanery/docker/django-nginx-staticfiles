#!/usr/bin/env python

from argparse import ArgumentParser
from datetime import datetime
from typing import Protocol
import subprocess


class Args(Protocol):
    base_tag: str
    no_cache: bool
    push: bool


def build_image(args: Args, tags: list[str]) -> int:
    docker_call_args: list[str] = []
    for tag in tags:
        docker_call_args.extend(("--tag", tag))
    if args.no_cache:
        docker_call_args.append("--no-cache")
    if args.push:
        docker_call_args.append("--push")

    return subprocess.call(["docker", "build", "--pull", *docker_call_args, "."])


def make_parser() -> ArgumentParser:
    parser = ArgumentParser(
        prog="build-images.py",
        description="Python base image builder for Docker",
    )

    parser.add_argument(
        '-t', '--base-tag', required=True,
        help="base tag of the resulting images (without image version)",
    )
    parser.add_argument(
        '-u', '--push', action='store_true', default=False,
        help="push all the images after the build is done",
    )
    parser.add_argument(
        '-c', '--no-cache', action='store_true', default=False,
        help="start from scratch, don't use cached layers",
    )

    return parser


def get_args() -> Args:
    return make_parser().parse_args()  # type: ignore


if __name__ == "__main__":
    args = get_args()
    build_image(args, [
        f"{args.base_tag}:{int(datetime.now().timestamp())}",
        f"{args.base_tag}:latest",
    ])
